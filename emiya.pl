#! /usr/bin/env perl

use strict;
use warnings;
use utf8;

use POE qw(Component::Client::HTTP);

use HTTP::Request;
use HTML::Entities;
use Encode;
use Digest::MD5 'md5_hex';
use Time::HiRes 'time';

use YAML::XS qw(LoadFile DumpFile);

use lib '.';
use PortIO;

our $VERSION = "0.9";
printout("Emiya $VERSION - <3 Saber\n");

printout("Loading settings... ");

if ( !file_exists("emiya.conf") ) {
	my $settings = {
		username => "PR",
		password => "",
		proxy    => "",
		move_to  => "Done",
		allow    => {
			title => [
				'\[Group\] Series - \d{1,3}v?\d? \([0-9A-Fa-f]{8}\)\.ext',
				'\[Group\] Series - EpNNNvN \(CRC32\)\.ext',
				'Batch_Folder_Name',
			],
		},
		deny               => { title => [], },
		max_display_length => "80",
		DELETE_ME          => "DELETE ME"
	};
	DumpFile( "emiya.conf", $settings );
	printout("Sample configuration saved on 'emiya.conf'.\n");
	printout("Edit it and then re-run this script.\n");
	exit 5;
}

my $settings = LoadFile("emiya.conf")
    || die "emiya.conf is invalid, delete it and re-run $0";

die "Default configuration detected. Edit emiya.conf and re-run this script"
    if $settings->{DELETE_ME};

my $username = $settings->{username};
my $password = $settings->{password};
my $proxy    = $settings->{proxy} || $ENV{http_proxy};
my $maxlen   = $settings->{max_display_length} || 80;

my $move_to = $settings->{move_to};

# FILTERS

# The main purpose of this script.
# Only files that pass any of the allow filters and fail all of the deny filters
# get downloaded.

# Use [0-9A-Fa-f]{8} for CRC32s
# Use \d{1,3}v?\d? for episode numbers (catches v2s and v3s as well)
# Remember to escape all '.', '[', ']', '(' and ')' on filenames with a '\'.

my %file_allow_filters = %{ $settings->{allow} };
my %file_deny_filters  = %{ $settings->{deny} };

printout("Compiling regexps... ");

# Compile regexps
foreach ( keys %file_allow_filters ) {
	foreach ( @{ $file_allow_filters{$_} } ) {
		$_ = qr/$_/;
	}
}
foreach ( keys %file_deny_filters ) {
	foreach ( @{ $file_deny_filters{$_} } ) {
		$_ = qr/$_/;
	}
}

printout("OK\n");

# HTTP Digest auth hack
my $realm  = "DeathWolf's Auto-Torrent Loving Realm";
my $A1     = md5_hex("$username:$realm:$password");
my $A2     = md5_hex("GET:/~DeathWolf/Recent/Latest-Files.html");
my $cnonce = int( time() );
my $reqid  = 1;

sub make_digest {
	my $www_auth = shift;

	$www_auth =~ /nonce=\"(.*?)\"/;
	my $nonce = $1;

	my $header = "Digest username=\"$username\", ";
	$header .= "realm=\"$realm\", nonce=\"$nonce\", ";
	$header .= "uri=\"/~DeathWolf/Recent/Latest-Files.html\", ";
	$header .= "qop=\"auth\", nc=$reqid, cnonce=\"$cnonce\", ";
	my $resp_digest = md5_hex("$A1:$nonce:$reqid:$cnonce:auth:$A2");
	$header .= "response=\"$resp_digest\"";
	return $header;
}

use constant CHUNK_SIZE => 16384;

{
	my $p = $proxy;
	$p = undef if ( !defined($proxy) or $proxy =~ m!^socks://! );
	POE::Component::Client::HTTP->spawn(
		Agent     => 'Emiya/0.2',
		Alias     => 'ua',
		Timeout   => 45,
		Streaming => CHUNK_SIZE,
		Proxy     => $p
	);
}

sub entity_clean {
	my $str = decode_utf8(shift);
	use bytes; $str =~ s/%([0-9A-Fa-f]{2})/@{[chr(hex($1))]}/g; no bytes;
	decode_entities($str);
	return $str;
}

sub shorten {
	my ( $str, $maxlen ) = @_;

	return $str if length($str) <= $maxlen;

	return undef if $maxlen < 3;
	return "..." if $maxlen == 3;
	return substr( $str, 0, 1 ) . "..." if $maxlen == 4;

	my $prelen  = ( $maxlen - 3 )/2;
	my $postlen = ( $maxlen - 3 ) - $prelen;

	my $toggle = 0;
	while ( $prelen > int($prelen) ) {
		if ( ( $toggle = !$toggle ) ) {
			$prelen -= 1/2;
			$prelen++;
		} else {
			$postlen -= 1/2;
			$postlen++;
		}
	}

	return substr( $str, 0, $prelen ) . "..." . substr( $str, -$postlen );
}

sub shorten_path {
	my ( $path, $maxlen ) = @_;
	if ( length($path) > $maxlen ) {
		my @paths = split( '/', $path );
		if ( scalar(@paths) < 2 ) {
			$path = shorten( $path, $maxlen );
		} else {
			$path = "";
			$maxlen -= length( $path = shift(@paths) . "/" )
			    if length( $paths[0] ) < $maxlen/2;

			my $count = scalar(@paths);
			my $len   = int( $maxlen/$count );
			my $extra = int($maxlen) % $count;
			my $last  = pop @paths;

			foreach (@paths) {
				$path .= shorten( "$_", $len - 1 ) . '/';
			}

			$path .= shorten( $last, $len + $extra );
		}
	}
	return $path;
}

POE::Session->create(
	inline_states => {

		# Makes initial request for the Digest authentication
		_start => sub {
			my ( $kernel, $heap ) = @_[ KERNEL, HEAP ];

			my $req
			    = HTTP::Request->new( GET =>
				    'http://saber.kawaii-shoujo.net/~DeathWolf/Recent/Latest-Files.html'
			    );
			$heap->{progress_message} = "Requesting List...";
			printout( $heap->{progress_message} . " " );
			$kernel->post( 'ua', 'request', 'authenticate', $req, 0 );
		},
		_stop => sub {
			my ($kernel) = $_[KERNEL];
			$kernel->post( 'ua', 'shutdown' );
		},

		# Authenticates and then actually fetch the list
		authenticate => sub {
			my $res = $_[ARG1]->[0];
			return
			    if defined $_[ARG1]->[1]
			; # Wait until the error page finishes downloading because of streaming
			my $req
			    = HTTP::Request->new( GET =>
				    'http://saber.kawaii-shoujo.net/~DeathWolf/Recent/Latest-Files.html'
			    );
			die $res->status_line unless ( $res->status_line =~ /^(401|2)/ );
			unless ( $res->is_success ) {
				my $header = make_digest( $res->header('WWW-Authenticate') );
				$req->header( 'Authorization', $header );
			}
			$_[KERNEL]->post( 'ua', 'request', 'stream_latest_files', $req,
				$reqid++, 'progress' );
		},
		progress => sub {
			my ( $heap, $done, $left ) = ( $_[HEAP], @{ $_[ARG1] }[ 0, 1 ] );
			my $msg = $heap->{progress_message};
			printout( sprintf( "%3.2f%%\r$msg ", $done*100/$left ) );
			printout("\n") if $done == $left;
		},

		# Puts the HTML in a temporary var
		stream_latest_files => sub {
			my ( $kernel, $session, $heap, $res, $data )
			    = ( @_[ KERNEL, SESSION, HEAP ], @{ $_[ARG1] } );

			die $res->status_line unless ( $res->is_success );

			if ( defined $data ) {
				$heap->{latest_files_data} = ""
				    unless exists $heap->{latest_files_data};
				$heap->{latest_files_data} .= $data;
			} else {
				$kernel->call( $session, 'parse_latest_files' );
				$kernel->yield('load_completed_files');
				$kernel->yield('filter_files');
			}
		},

		# Parses the HTML, puts hashrefs in $_[HEAP]->{latest_files}
		parse_latest_files => sub {
			my $heap = $_[HEAP];

			my $content = $heap->{latest_files_data};
			delete $heap->{latest_files_data};
			my @files = ();
			my $info  = {};

			my @lines = split m/\n/, $content;

			foreach (@lines) {
				if (m/^\s*(<td.*)/) {
					$_ = $1;
					if (m!class="date".*?>(.+?)</td>!) {
						$info->{date} = $1;
					} elsif (m!class="category".*?>(.+?)</td>!) {
						$info->{category} = $1;
					} elsif (m!class="size".*?>(.+?)</td>!) {
						$info->{size} = $1;
					} elsif (m!<td><a href="(.+?)">(.+?)</a></td>!) {
						$info->{url}   = entity_clean($1);
						$info->{title} = entity_clean($2);
					}
				} elsif ( m!^\s*</tr>! and $info->{url} ) {
					push @files, $info;
					$info = {};
				}
			}

			$heap->{latest_files} = \@files;
		},

		# Loads the list of completed files
		load_completed_files => sub {
			my $heap = $_[HEAP];
			$heap->{completed_files} = [];
			if ( file_exists("completed_files.txt") ) {
				my $in = file_open( "<", "completed_files.txt" );
				foreach (<$in>) {
					chomp;
					s/\r$//;
					push @{ $heap->{completed_files} }, decode_utf8($_);
				}
				close $in;
			}
		},
		mark_as_completed => sub {
			my $completed_files = file_open( ">>", "completed_files.txt" );
			print $completed_files encode_utf8( $_[ARG0] . "\n" );
			close $completed_files;
		},

		# Filters the file list
		filter_files => sub {
			my ( $kernel, $heap ) = @_[ KERNEL, HEAP ];
			$heap->{allowed_files} = [];
			my $skip;
			my $filecount = scalar( @{ $heap->{latest_files} } );
			my $i         = 1;
			foreach my $info ( @{ $heap->{latest_files} } ) {
				printout( "\rFiltering " . $i++ . "/$filecount... " );
				$skip = 0;
				foreach ( @{ $heap->{completed_files} } ) {
					if ( $info->{title} eq $_ ) {
						$skip = 1;
						last;
					}
				}
				next if $skip;
				my $allowed = 0;
				foreach my $group ( keys %file_allow_filters ) {
					foreach my $filter ( @{ $file_allow_filters{$group} } ) {
						if ( $info->{$group} =~ /$filter/ ) {
							$allowed = 1;
							last;
						}
					}
					last if $allowed;
				}

				foreach my $group ( keys %file_deny_filters ) {
					foreach my $filter ( @{ $file_deny_filters{$group} } ) {
						if ( $info->{$group} =~ /$filter/ ) {
							$allowed = 0;
							last;
						}
					}
					last unless $allowed;
				}

				push @{ $heap->{allowed_files} }, $info if $allowed;
			}
			delete $heap->{latest_files};
			$kernel->yield('sort_allowed_files');
			$kernel->yield('print_allowed_files');
			$kernel->yield('download_entries');
		},
		sort_allowed_files => sub {

			# TODO: actually sort them
			# NOTE: the list is used in reverse order
		},
		print_allowed_files => sub {
			my $heap = $_[HEAP];
			my $a = @{ $heap->{allowed_files} } > 0 ? ":" : ".";
			printout( @{ $heap->{allowed_files} } . " entries accepted$a\n" );
			foreach ( @{ $heap->{allowed_files} } ) {
				printout( "  \""
					    . $_->{title}
					    . ( $_->{url} =~ m!/$! ? "/" : "" ) . "\" ("
					    . $_->{size}
					    . ")\n" );
			}
		},

		# This state is called after each file finishes downloading
		download_entries => sub {
			my ( $kernel, $session, $heap ) = @_[ KERNEL, SESSION, HEAP ];
			my $info = pop @{ $heap->{allowed_files} };
			if ($info) {
				if ( $info->{url} =~ /\/$/ ) {
					$kernel->call( $session, 'subfolder_recurse', $info );
				} else {
					$kernel->call( $session, 'download_file', $info );
				}
			}
		},

		# Downloads folder listings
		subfolder_recurse => sub {
			my ( $kernel, $heap, $info ) = @_[ KERNEL, HEAP, ARG0 ];

			my $req = HTTP::Request->new( GET => $info->{url} );
			$heap->{info} = $info;
			$heap->{progress_message}
			    = "Listing \"" . $info->{title} . "/\":";
			$kernel->call( 'ua', 'request', 'stream_subfolder', $req,
				$reqid++, 'progress' );
			while ( defined $heap->{info} ) { $kernel->run_one_timeslice(); }
			$kernel->yield('download_entries');
		},
		stream_subfolder => sub {
			my ( $kernel, $heap, $res, $data )
			    = ( @_[ KERNEL, HEAP ], @{ $_[ARG1] } );
			my $info = $heap->{info};

			die $res->status_line unless ( $res->is_success );

			# HACK: I dunno why I use md5_hex, but it looked like a good idea at the time
			# HACK: A very bizarre and most certainly broken filename made perl whine, so I added encode_entities
			if ( defined $data ) {
				$heap->{ "folder-"
					    . md5_hex( encode_entities( $info->{title} ) ) } = ""
				    unless exists $heap->{
						    "folder-"
						        . md5_hex( encode_entities( $info->{title} ) )
					    };
				$heap->{ "folder-"
					    . md5_hex( encode_entities( $info->{title} ) ) }
				    .= $data;
			} else {
				$kernel->yield('subfolder_parse');
			}
		},

		# Parses folder listings, pushes subitems to allowed_files, folders last
		# This makes subfolders be checked before files
		subfolder_parse => sub {
			my $heap = $_[HEAP];

			my $info  = $heap->{info};
			my @lines = split m/\n/,
			    $heap->{ "folder-"
				    . md5_hex( encode_entities( $info->{title} ) ) };
			my @files   = ();
			my @folders = ();
			foreach (@lines) {
				if (m!<td class="n"><a href="(.*?)">.*<td class="t">Directory</td>!
				    ) {
					next if $1 eq "../";
					push @folders, $1;
				} elsif (
					m!<td class="n"><a href=".*?">(.*?)</a></td>.*<td class="s">(.*?)</td>!
				    ) {
					push @files, [ $1, $2 ];
				}
			}

			while ( $_ = pop @files ) {
				my $finfo = { %{$info} };
				$finfo->{title} = $info->{title};
				$finfo->{title} .= '/' unless $finfo->{title} =~ m!/$!;
				$finfo->{title} .= entity_clean( $_->[0] );
				$finfo->{url}      = $info->{url} . entity_clean( $_->[0] ),
				    $finfo->{size} = $_->[1];

				my $skip = 0;
				foreach ( @{ $heap->{completed_files} } ) {
					if ( $finfo->{title} eq $_ ) {
						$skip = 1;
						last;
					}
				}
				push @{ $heap->{allowed_files} }, $finfo unless $skip;
			}

			# There is no trailing slash on the title
			# There IS a trailing slash on folder URLs
			foreach (@folders) {
				my $subinfo = { %{$info} };    # make a copy
				$subinfo->{title} = $info->{title};
				$subinfo->{title} .= '/' unless $subinfo->{title} =~ m!/$!;
				$subinfo->{title} .= entity_clean($_);
				$subinfo->{title} .= '/' unless $subinfo->{title} =~ m!/$!;
				$subinfo->{url} = $info->{url} . entity_clean($_);

				my $skip = 0;
				foreach ( @{ $heap->{completed_files} } ) {
					if ( $subinfo->{title} eq $_ ) {
						$skip = 1;
						last;
					}
				}
				push @{ $heap->{allowed_files} }, $subinfo unless $skip;
			}

			delete $heap->{ "folder-"
				    . md5_hex( encode_entities( $info->{title} ) ) };
			$heap->{info} = undef;
			delete $heap->{info};
		},

		# Actually downloads files
		download_file => sub {
			my ( $kernel, $heap, $info ) = @_[ KERNEL, HEAP, ARG0 ];

			my $req = HTTP::Request->new( GET => $info->{url} );
			$req->header( Accept_Ranges => "bytes" );
			$heap->{have_size} = file_size( $info->{title} );
			if ( $heap->{have_size} ) {
				$req->header( Range => "bytes=" . $heap->{have_size} . "-" );
			} else {
				$heap->{have_size} = 0;
			}
			my $sizelen = length( $info->{size} ) + 2;
			my $proglen = 23;
			$heap->{progress_message} = "\""
			    . shorten_path( $info->{title},
				$maxlen - $sizelen - $proglen - 3 )
			    . "\" ("
			    . $info->{size} . "): ";
			printout( $heap->{progress_message} );

			$heap->{filename} = $info->{title};

			$heap->{last_time}  = time;
			$heap->{start_time} = $heap->{last_time};
			$heap->{len}        = 0;
			$heap->{oct}        = 0;
			$kernel->delay( 'calculate_speed' => 2 );
			$kernel->call( 'ua', 'request', 'stream_file', $req, $reqid++,
				'file_progress' );
		},

		# Does the actual file downloading
		stream_file => sub {
			my ( $kernel, $session, $heap, $req, $res, $data )
			    = ( @_[ KERNEL, SESSION, HEAP, ARG0 ], @{ $_[ARG1] } );

			die $res->status_line
			    unless $res->is_success
				    or $res->status_line =~ /^416/;

			if ( defined($data) ) {
				unless ( $heap->{outfh} and $heap->{content_length} ) {
					$heap->{filename} =~ m!(.*)/!;
					mkpath($1) if $1;
					$heap->{outfh} = file_open( '>>', $heap->{filename} )
					    or die "$^E";
					$heap->{content_length} = $res->header('Content-Length');

					# This is probably wrong
					if ( $heap->{content_length} == $heap->{have_size} ) {
						close( $heap->{outfh} );
						$kernel->call( $session, 'mark_as_completed',
							$heap->{filename} );
						$kernel->post( 'ua', 'cancel', $req );
						$kernel->yield('download_entries');
					}
				}
				unless ( $heap->{outfh} && syswrite( $heap->{outfh}, $data ) )
				{
					die "$^E";
				}
			} else {    # Finished
				unless ( $res->is_success or $res->status_line =~ /^416/ )
				{       # Requested Range not satisfiable
					die $res->status_line;
				}
				close( $heap->{outfh} );
				delete $heap->{outfh};
				$kernel->call( $session, 'mark_as_completed',
					$heap->{filename} );
				$kernel->call( $session, 'move_completed',
					$heap->{filename} );
				$kernel->yield('download_entries');
			}
		},
		calculate_speed => sub {
			my ( $kernel, $heap ) = @_[ KERNEL, HEAP ];

			my $now = time;
			if ( $now == $heap->{last_time} ) {
				$kernel->delay( 'calculate_speed' => 0.5 );
				return;
			}
			my $spd = $heap->{oct}/1024/( $now - $heap->{last_time} );
			unless ( defined $heap->{speeds} ) {
				$heap->{speeds}   = [];
				$heap->{speeds_i} = 0;
				for ( my $i = 0; $i < 8; $i++ ) {
					$heap->{speeds}->[$i] = $spd;
				}
			}
			$heap->{speeds}->[ $heap->{speeds_i}++ ] = $spd;
			$heap->{speeds_i} %= @{ $heap->{speeds} };
			$heap->{oct} = 0;

			$heap->{spd} = 0;
			foreach ( @{ $heap->{speeds} } ) { $heap->{spd} += $_; }
			$heap->{spd} /= @{ $heap->{speeds} };

			$heap->{last_time} = time;
			$kernel->delay( 'calculate_speed' => 0.5 );
		},
		calculate_avg_speed => sub {
			my $heap = $_[HEAP];

			my $now = time;
			$heap->{spd} = $heap->{len}/1024/( $now - $heap->{start_time} );
		},

		# Prints the progress report
		file_progress => sub {
			my ( $kernel, $heap, $pos, $len, $oct )
			    = ( @_[ KERNEL, HEAP ], @{ $_[ARG1] } );

			if ( $pos eq $len ) {
				$kernel->call( $_[SESSION], 'calculate_avg_speed' );
				my $line
				    = sprintf( "100.00%% (avg. %4.2f kB/s)", $heap->{spd} );
				$heap->{prevlen} = 0 unless $heap->{prevlen};
				printout( "\r"
					    . $heap->{progress_message} . "$line"
					    . ( " " x ( $heap->{prevlen} - length($line) ) )
					    . "\n" );
				delete $heap->{have_size};
				delete $heap->{prev_time};
				delete $heap->{prevlen};
				delete $heap->{spd};
				delete $heap->{speeds};
				$heap->{speeds_i} = 0;
				$kernel->delay('calculate_speed');
			} else {
				my $have_size
				    = exists $heap->{have_size} ? $heap->{have_size} : 0;

				my $spd = $heap->{spd} ? $heap->{spd} : undef;

				my $octlen = length($oct);
				$heap->{oct} += $octlen;
				$heap->{len} += $octlen;

				# $have_size may be a BigInt, use fixed point math / formatters
				my $pct = ( $pos + $have_size )*10000/( $len + $have_size );

				my $line;
				if ( defined($spd) ) {
					$line = sprintf( "%2d.%02d%% (%4.2f kB/s)",
						$pct/100, $pct % 100, $spd );
				} else {
					$line = sprintf( "%2d.%02d%%", $pct/100, $pct % 100 );
				}
				$heap->{prevlen} = length($line)
				    unless defined $heap->{prevlen}
					    and $heap->{prevlen} > length($line);
				printout( "$line"
					    . ( " " x ( $heap->{prevlen} - length($line) ) )
					    . "\r"
					    . $heap->{progress_message} );
				$heap->{prevlen} = length($line);
			}
		},
		move_completed => sub {
			my $filename = $_[ARG0];
			if ($move_to) {
				my $dest = $move_to;
				my $fname;
				$filename =~ m!^(.+)/(.+?)$!;
				if ($1) {
					$dest .= "/$1";
					$fname = $2;
				} else {
					$fname = $filename;
				}
				mkpath("$dest");
				printout("Moving \"$filename\" to \"$dest/$fname\"... ");
				if ( move( "$filename", "$dest/$fname" ) ) {
					printout("OK\n");
				} else {
					printout("$^E\n");
				}
			}
		    }
    } );

POE::Kernel->run();

# vim: set noexpandtab tabstop=4 shiftwidth=4 :
